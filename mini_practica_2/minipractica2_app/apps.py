from django.apps import AppConfig


class Minipractica2AppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "minipractica2_app"

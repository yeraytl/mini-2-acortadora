from django.shortcuts import render
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
import urllib.parse
from django.template import loader


@csrf_exempt
def form_and_list(request):
    contenidos = Contenido.objects.all()

    if request.method == "GET":
        template = loader.get_template('app/form.html')
    context = {'contenidos':contenidos,}

    if request.method == "POST":
        peticion_partida = request.body.decode('utf-8').split("&")
        if peticion_partida[0] == "url_original=" and peticion_partida[1] == "url_acortada=":
            template = loader.get_template('app/formVacioError.html')
            return HttpResponse(template.render())
        else:
            url = urllib.parse.unquote(peticion_partida[0].split("=")[1])
            if not url.startswith('http://') and not url.startswith('https://'):
                url = "https://" + url
            acortada = peticion_partida[1].split("=")[1]
            c = Contenido(url=url, url_acortada=acortada)
            c.save()

            template = loader.get_template('app/urls_con_acortadas.html')

    return HttpResponse(template.render(context, request))

    return HttpResponse(respuesta)


@csrf_exempt
def mirar_contenido(request, url_acortada):
    contenido = Contenido.objects.get(url_acortada=url_acortada)
    template = loader.get_template('app/redirect.html')
    context = {'content': contenido}
    return HttpResponse(template.render(context, request))
